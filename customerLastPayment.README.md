Consider the database structure below:

+-------------------+-------------------+
| customer				|
+-------------------+-------------------+
| Field		    | Type		|
+-------------------+-------------------+
| id		(PK)| int		|
| name		    | varchar(20)	|
+-------------------+-------------------+

+-------------------+-------------------+
| payment				|
+-------------------+-------------------+
| Field		    | Type		|
+-------------------+-------------------+
| id 		(PK)| int		|
| open_date	    | date		|
| status	    | char(2)		|
| customer_id	    | int		|
+-------------------+-------------------+

Customer data

1|Matt
2|John

Payment data

1|2015-10-10|CO|1
2|2015-10-10|CA|1
3|2015-11-20|CO|2
4|2016-02-15|CO|1
5|2016-04-02|CA|2
6|2016-08-30|CO|1
7|2016-09-10|PE|1

Problem:
Write a SQL query that returns the customer name and the last payment_id for each one, where payment status is equal to 'CO'.

Expected Output:

John 3
Matt 6
