create table customer (
    id number not null primary key,
    name varchar(20)
);

create table payment (
    id number not null primary key,
    open_date date,
    status varchar(2),
    customer_id int
);

INSERT INTO customer VALUES (1, "Matt"), (2, "John") ;
INSERT INTO payment VALUES (1, "2015-10-10", "CO", 1), (2, "2015-10-10", "CA", 1 ), (3, "2015-11-20", "CO", 2), (4, "2016-02-15", "CO", 1), (5, "2016-04-02", "CA", 2), (6, "2016-08-30", "CO", 1), (7, "2016-09-10", "PE", 1);


select * from customer;

select * from payment;


select customer.name, payment.id
    from customer
        join payment
            on customer.id=payment.customer_id
            where payment.status="CO"
            group by customer.id
